# Overview

## PC

* [ariya/phantomjs](https://github.com/ariya/phantomjs) -- phantomjs is a headless WebKit scriptable with JavaScript.    

* [SeleniumHQ/selenium](https://github.com/SeleniumHQ/selenium) -- Selenium is an umbrella project encapsulating a variety of tools and libraries enabling web browser automation.         

* [cucumber/cucumber](https://github.com/cucumber/cucumber) -- Cucumber is a tool that supports Behaviour-Driven Development (BDD) - a software development process that aims to enhance software quality and reduce maintenance costs.

* [theintern/intern](https://github.com/theintern/intern) -- Intern is a complete test system for JavaScript designed to help you write and run consistent, high-quality test cases for your JavaScript libraries and applications.         

* [Alibaba/f2etest](https://github.com/alibaba/f2etest) -- F2etest是一个面向前端、测试、产品等岗位的多浏览器兼容性测试整体解决方案。

* [robotframework/robotframework](./docs/pc/robotframework.md) -- Robot Framework is a generic open source test automation framework for acceptance testing and acceptance test-driven development (ATDD).

* [alibaba/macaca](https://github.com/alibaba/macaca) -- Solution for Automation Test with Ease   

* [dalekjs/dalek](https://github.com/dalekjs/dalek) -- Automated cross browser testing with JavaScript!Exterminate all the bugs!      

* [watir/watir](https://github.com/watir/watir) -- What is an open source Ruby library for automating tests. Watir interacts with a browser the same way people do: clicking links, filling out forms and validating text. 

* [totorojs/totoro](https://github.com/totorojs/totoro) -- A simple and stable cross-browser testing tool.     

* [NetEase/Dagger](https://github.com/NetEase/Dagger) -- a light, robust Web UI autotest framework     

* [IndigoUnited/automaton](https://github.com/IndigoUnited/automaton) -- Task automation tool built in JavaScript.  

* [catjsteam/catjs](https://github.com/catjsteam/catjs) -- Mobile web automation testing framework. Unit tests can be added to any HTML5 application easily. With catjs you get to be Agile.



## Mobile

* [appium/appium](https://github.com/appium/appium) -- Appium is an open source, cross-platform test automation tool for native, hybrid and mobile web apps, tested on simulators (iOS, FirefoxOS), emulators (Android), and real devices (iOS, Android, Windows, FirefoxOS).    

* [kif-framework/KIF](https://github.com/kif-framework/KIF) -- Keep It Functional - An iOS Functional Testing Framework    

* [google/EarlGrey](https://github.com/google/EarlGrey) -- EarlGrey is a native iOS UI automation test framework that enables you to write clear, concise tests.

* [robolectric/robolectric](https://github.com/robolectric/robolectric) -- Robolectric is a testing framework that de-fangs the Android SDK so you can test-drive the development of your Android app.    

* [RobotiumTech/robotium](https://github.com/RobotiumTech/robotium)  -- User scenario testing for Android

* [calabash/calabash-ios and andriod](https://github.com/calabash/calabash-ios) -- Calabash is an automated testing technology for Android and iOS native and hybrid applications.

* [TestingWithFrank/Frank](https://github.com/moredip/Frank) -- Automated acceptance tests for native iOS apps

* [NetEase/Emmagee](https://github.com/NetEase/Emmagee) -- Android performance test tool-CPU,memory,network traffic,starting time,battery current and status

* [pivotal/cedar](https://github.com/pivotal/cedar) -- Cedar is a BDD-style Objective-C testing framework with an expressive matcher DSL and convenient test doubles.

* [sikuli/sikuli](https://github.com/sikuli/sikuli) -- Sikuli is a visual technology to automate graphical user interfaces (GUI) using images (screenshots).

* [selendroid/selendroid](https://github.com/selendroid/selendroid) -- "Selenium for Android" (Test automate native or hybrid Android apps and the mobile web with Selendroid.) 

* [BaiduQA/Cafe](https://github.com/BaiduQA/Cafe) -- A powerful test framework for Android named Case Automated Framework for Everyone.

* [KeepSafe/Switchboard](https://github.com/KeepSafe/Switchboard) -- Switchboard - easy and super light weight A/B testing for your mobile iPhone or android app. This mobile A/B testing framework allows you with minimal servers to run large amounts of mobile users.

* [robospock/RoboSpock](https://github.com/robospock/RoboSpock) -- A testing framework which brings powers of Spock and Groovy to Android app testing







